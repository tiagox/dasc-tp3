#!/bin/sh

javac *.java

if [ $? -ne 0 ]
then
	exit 1
fi

cp Account.class IfaceRemoteClass1.class RemoteClass1.class StartRemoteObject.class SubscrbrCustomerIface.class server
cp Account.class AskRemote1.class IfaceRemoteClass1.class SubscrbrCustomerIface.class SubscrbrCustomer.class client
rm *.class

echo "Compiled successfully."

/*
 * SubscrbrCustomerIface: interface of a "subscriber" customer
 *
 */

import java.rmi.*;

public interface SubscrbrCustomerIface extends Remote {
  // This remote method is invoked as a callback by a "classic server"...
  public String notifyMe(String message) throws RemoteException;
}

/*
 * Account class... just a minimum one
 */

import java.io.Serializable;

/* A "classical class"... */
public class Account implements Serializable {
  protected int id;
  protected String acctype;

  public Account(int idpar, String acctypepar) {
    id = idpar;
    acctype = acctypepar;
  }
}

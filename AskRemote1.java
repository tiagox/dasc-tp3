/*
 * AskRemote.java
 *     a) Creates an object for callback
 *     b) Looks up for the remote object
 *     c) "Makes" the RMI, sending the callback-enabled locally created object
 */

import java.rmi.Naming;
import java.rmi.registry.Registry;

public class AskRemote1 {
  public static void main(String[] args) {
    /* Look for hostname and msg length in the command line */
    if (args.length != 2) {
      System.out.println("2 argument needed: (remote) hostname and client identifier");
      System.exit(1);
    }

    try {
      String rname = "//" + args[0] + ":" + Registry.REGISTRY_PORT + "/remote";

      IfaceRemoteClass1 remoteObj = (IfaceRemoteClass1) Naming.lookup(rname);

      SubscrbrCustomerIface newCust = new SubscrbrCustomer(args[1], 1);

      Account localAcc = remoteObj.regObj(newCust);

      System.out.println("Subscribed!");
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}

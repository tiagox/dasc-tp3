/*
 * RemoteClass1.java
 *    Just implements the RemoteMethod interface as a class extension to
 *    UnicastRemoteObject
 *
 *    A "standard" method makes a single RMI callback if possible...
 */

/* Needed for implementing remote method/s */
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.LinkedList;
import java.util.List;

/* This class implements the interface with remote methods */
public class RemoteClass1 extends UnicastRemoteObject implements IfaceRemoteClass1 {

  protected List<SubscrbrCustomerIface> subscribers;

  protected RemoteClass1() throws RemoteException {
    super();

    subscribers = new LinkedList<SubscrbrCustomerIface>();
  }

  /* Remote method implementation */
  public Account regObj(SubscrbrCustomerIface custpar) throws RemoteException {
    /* Just to return something... and always the same contents... */
    Account newAcc = new Account(1, "notype");

    System.out.println("Registering object sent from somewhere...");

    subscribers.add(custpar);

    try {
      Thread.sleep(5_000);
    } catch (InterruptedException ex) {
    }

    return newAcc;
  }

  public int makeCallBack() {
    if (subscribers.size() > 0) {
      for (SubscrbrCustomerIface subscriber : subscribers) {
        new Thread(
                () -> {
                  try {
                    subscriber.notifyMe(
                        "Callback executed from thread n# " + Thread.currentThread().getId());
                  } catch (RemoteException e) {
                    e.printStackTrace();
                  }
                })
            .start();
      }
    }
    return subscribers.size();
  }
}

/*
 * SubscrbrCustomer class enabled for remote invocation <==> subscriber...
 *
 */

/* Needed for remote invocation on these objects... */
import java.rmi.*;
import java.rmi.server.*;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class SubscrbrCustomer extends UnicastRemoteObject implements SubscrbrCustomerIface {
  protected String name;
  protected int id;

  public SubscrbrCustomer(String namepar, int idpar) throws RemoteException {
    super();
    name = namepar;
    id = idpar;
  }

  private String getTimestamp() {
    return LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss"));
  }

  public String notifyMe(String message) {
    String returnMessage =
        "Call back received: at " + name + " with message => \"" + message + "\"";
    System.out.println(getTimestamp() + " | " + returnMessage);

    try {
      Thread.sleep(10_000);
    } catch (InterruptedException ex) {
    }

    System.out.println(getTimestamp() + " | Finish process with message => \"" + message + "\"");

    return returnMessage;
  }
}
